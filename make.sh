#!/bin/bash

MAKE_THEARD=`cat /proc/cpuinfo| grep "processor"| wc -l`
MAKE_TOTAL=`cat /proc/meminfo| grep "MemTotal" |  tr -cd "[0-9]"`
MAKE_MEM=` expr $MAKE_TOTAL / 1048576`
#echo $MAKE_MEM

jvm_set_mem()
{
        if [ $MAKE_MEM -gt 32 ]
                then
                {
                        export JACK_SERVER_VM_ARGUMENTS="-Dfile.encoding=UTF-8 -XX:+TieredCompilation -Xmx32768m"
                }
        elif [ $MAKE_MEM -gt 16 ]
                then
                {
                        export JACK_SERVER_VM_ARGUMENTS="-Dfile.encoding=UTF-8 -XX:+TieredCompilation -Xmx16384m"
                }
        elif [ $MAKE_MEM -gt 8 ]
                then
                {
                        export JACK_SERVER_VM_ARGUMENTS="-Dfile.encoding=UTF-8 -XX:+TieredCompilation -Xmx8192m"
                }
        elif [ $MAKE_MEM -ge 6 ]
                then
                {
                        export JACK_SERVER_VM_ARGUMENTS="-Dfile.encoding=UTF-8 -XX:+TieredCompilation -Xmx4096m"
                }
        else
                echo "/***************************************************/" 
                echo "memory less than 7GB, can not compile" 
                echo "memory less than 7GB, can not compile" 
                echo "memory less than 7GB, can not compile" 
                echo "memory less than 7GB, can not compile" 
                echo "memory less than 7GB, can not compile" 
                echo "memory less than 7GB, can not compile" 
                echo "memory less than 7GB, can not compile" 
                echo "memory less than 7GB, can not compile" 
                echo "memory less than 7GB, can not compile" 
                echo "memory less than 7GB, can not compile" 
                echo "memory less than 7GB, can not compile" 
                echo "memory less than 7GB, can not compile" 
                echo "memory less than 7GB, can not compile" 
                echo "memory less than 7GB, can not compile" 
                echo "memory less than 7GB, can not compile" 
                echo "memory less than 7GB, can not compile" 
                echo "memory less than 7GB, can not compile" 
                echo "/***************************************************/" 
                exit 1
        fi
./prebuilts/sdk/tools/jack-admin start-server
}

path_set()
{
	export JAVA_HOME=/usr/lib/jvm/java-8-openjdk-amd64
	export JRE_HOME=/usr/lib/jvm/java-8-openjdk-amd64/jre
	export PATH=$JAVA_HOME/bin:$PATH
	export CLASSPATH=.:$JAVA_HOME/lib:$JAVA_HOME/lib/tools.jar

	source build/envsetup.sh
	lunch rk3126c-userdebug
}

if [ $1 == "clean" ]
	then
	{
		if [ $2 == "u-boot"  -o $2 == "u-boot/" -o $2 == "uboot" ]
			then
			{
				echo make clean u-boot
				pushd u-boot/
				make distclean
				popd
				pushd linux_update/rockdev/
				rm MiniLoaderAll.bin
				popd
				pushd linux_update/rockdev/Image/
				rm uboot.img
				popd
		}
		elif [ $2 == "kernel" -o $2 == "kernel/" ]
			then
			{
        			echo make clean kernel
				pushd kernel/
				make clean
				popd
				pushd linux_update/rockdev/Image/
				rm kernel.img resource.img
				popd
		}
		elif [ $2 == "android" -o $2 == "android/" ]
			then
			{
        			echo make clean android
				path_set
				make clean
				pushd linux_update/rockdev/Image/
				rm misc.img boot.img recovery.img system.img
				popd	
				rm out -rf
			}
		else
			{
        			echo make clean u-boot
				pushd u-boot/
				make distclean
				popd
        			echo make clean kernel
				pushd kernel/
				make clean
				popd
        			echo make clean android
				path_set
				make clean
				rm out -rf
				pushd rockdev/Image-rk3399_mid/
				rm *.img
				popd
				popd linux_update/rockdev/
				rm MiniLoaderAll.bin update.img
				popd
				pushd linux_update/rockdev/Image/
				rm kernel.img resource.img misc.img boot.img recovery.img system.img update.img uboot.img
				popd
				echo clean Img oK
			}
		fi
	}
elif [ $1 == "u-boot" -o $1 == "u-boot/"  -o $1 == "uboot" ]
	then
	{
		pushd u-boot/
		#make rk3128_box_defconfig 
		make rk3128_defconfig
		make -j12
		if [ $? -eq 0 ]; then
			echo "Build uboot ok!"
		else
			echo "Build uboot failed!"
			exit 1
		fi
		popd
		#path_set
		#./mkimage.sh
	}
elif [ $1 == "kernel" -o $1 == "kernel/" ]
	then
	{
		pushd kernel/
		convert -quality 92 -depth 8 logo.png logo.bmp
		convert -quality 92 -depth 24 -flip logo.png logo_kernel.bmp
		#make rockchip_defconfig 
		make rp-rk3128.img -j8
		if [ $? -eq 0 ]; then
			echo "Build kernel ok!"
		else
			echo "Build kernel failed!"
			exit 1
		fi
		popd
		cp kernel/resource.img rockdev/Image-rk3126c/
		cp kernel/kernel.img rockdev/Image-rk3126c/
		#path_set
		#./mkimage.sh
	}
elif [ $1 == "android" -o $1 == "android/" ]
	then
	{
		jvm_set_mem
		path_set
		make -j $MAKE_THEARD  
		if [ $? -eq 0 ]; then
			echo "Build android ok!"
		else
			echo "Build android failed!"
			exit 1
		fi
		./mkimage.sh 
	}
elif [ $1 == "ota" ]
	then
	{
		path_set
		./mkimage.sh ota
	}
else
	{
		pushd u-boot/
		#make rk3128_box_defconfig 
		make rk3128_defconfig
		make -j8
		if [ $? -eq 0 ]; then
			echo "Build uboot ok!"
		else
			echo "Build uboot failed!"
			exit 1
		fi
		popd
		
		pushd kernel/ 
		#make rockchip_defconfig
		make rp-rk3128.img -j8
		if [ $? -eq 0 ]; then
			echo "Build kernel ok!"
		else
			echo "Build kernel failed!"
			exit 1
		fi
		popd
		jvm_set_mem
		path_set
		make -j $MAKE_THEARD  
		if [ $? -eq 0 ]; then
			echo "Build android ok!"
		else
			echo "Build android failed!"
			exit 1
		fi
		./mkimage.sh 
		. rpupdate.sh
	}
fi

