include device/rockchip/rk312x/BoardConfig.mk

TARGET_BOARD_PLATFORM := rk3126c
BOARD_HAVE_BLUETOOTH := true
BOARD_SUPPORT_HDMI := true
BOARD_USE_SPARSE_SYSTEM_IMAGE := true
PRODUCT_PACKAGES += \
        rild \
        Launcher3 \
        WallpaperPicker \
        SoundRecorder
