LOCAL_PATH := $(call my-dir)
include $(CLEAR_VARS)

LOCAL_SRC_FILES := spidev_test.c

LOCAL_C_INCLUDES := spidev_test.h

LOCAL_MODULE := spidev_test

LOCAL_MODULE_TAGS := optional

include $(BUILD_EXECUTABLE)
