#!/system/bin/sh
	su
#if [ -e /system/usr/superuser/root_enable/Superuser.apk ] && [ -e /system/usr/superuser/root_enable/install-recovery.sh ] && [ -e /system/usr/superuser/root_enable/su ] && [ -e /system/usr/superuser/root_enable/supolicy ] && [ -e /system/usr/superuser/root_enable/libsupol.so ] && [ ! -e /system/app/SuperSU/SuperSU.apk]; then
#if [ ! -e /system/app/SuperSU/SuperSU.apk ]; then
	cp /system/usr/superuser/root_enable/Superuser.apk /system/app/SuperSU/SuperSU.apk
	cp /system/usr/superuser/root_enable/install-recovery.sh /system/etc/install-recovery.sh
	cp /system/usr/superuser/root_enable/su	/system/bin/.ext/.su
	cp /system/usr/superuser/root_enable/su	/system/xbin/su
	cp /system/usr/superuser/root_enable/su	/system/xbin/daemonsu
	cp /system/usr/superuser/root_enable/su /system/xbin/sugote
	cp /system/usr/superuser/root_enable/supolicy /system/xbin/supolicy
	cp /system/usr/superuser/root_enable/libsupol.so /system/lib/libsupol.so
	chmod 0644 /system/app/SuperSU/SuperSU.apk
	chmod 0755 /system/etc/install-recovery.sh 
	chmod 0755 /system/bin/.ext/.su 
	chmod 0755 /system/xbin/daemonsu 
	chmod 0755 /system/xbin/su
	chmod 0755 /system/xbin/sugote 
	chmod 0755 /system/xbin/supolicy 
	chmod 0755 /system/lib/libsupol.so 
	touch /system/etc/.installed_su_daemon 
	chmod 0644 /system/etc/.installed_su_daemon 
	cp /system/bin/sh /system/xbin/sugote-mksh 
	chmod 0755 /system/xbin/sugote-mksh 

	cp /system/usr/superuser/root_enable/app_process32 /system/bin/app_process32_original 
	chmod 0755 /system/bin/app_process32_original 
	cp /system/usr/superuser/root_enable/app_process /system/bin/app_process_original 
	chmod 0755 /system/bin/app_process_original 
	cp /system/usr/superuser/root_enable/app_process32 /system/bin/app_process_init 
	chmod 0755 /system/bin/app_process_init 

	rm /system/bin/app_process
	rm /system/bin/app_process32
	ln -s /system/xbin/daemonsu /system/bin/app_process 
	ln -s /system/xbin/daemonsu /system/bin/app_process32 
	ln -s /system/etc/install-recovery.sh /system/bin/install-recovery.sh
#else
#	echo "miss SuperSU file, failed to set SuperSU"
#fi