#!/system/bin/sh

if [ -e /system/app/SuperSU/SuperSU.apk ]; then
	su
	rm /system/etc/.installed_su_daemon 
	rm /system/xbin/sugote-mksh 
if [ -e /system/bin/app_process32_original ] && [ -L /system/bin/app_process ] && [ -L /system/bin/app_process32 ]; then
	rm /system/bin/app_process_init 
	rm /system/bin/app_process 
	rm /system/bin/app_process32

	mv  /system/bin/app_process_original /system/bin/app_process
	mv  /system/bin/app_process32_original /system/bin/app_process32
fi 
	rm /system/bin/install-recovery.sh
	rm /system/app/SuperSU/SuperSU.apk
	rm /system/etc/install-recovery.sh
	rm /system/bin/.ext/.su
	rm /system/xbin/su
	rm /system/xbin/daemonsu
	rm /system/xbin/sugote
	rm /system/xbin/supolicy
	rm /system/lib/libsupol.so
fi
	chmod 4777 /system/usr/superuser/root_disable/su
	chmod 777 /system/xbin
	chmod 4777 /system/xbin/su
	cp /system/usr/superuser/root_disable/su /system/xbin/su
	chmod 4777 /system/xbin/su