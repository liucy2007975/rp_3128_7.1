/dts-v1/;

#include "rk3128.dtsi"
#include "rk3128-cif-sensor.dtsi"
//#include "lcd-rpdzkj-lvds-10_1_1024_600.dtsi"
//#include "lcd-rpdzkj-mipi-5_720_1280.dtsi"
//#include "lcd-rpdzkj-mipi-7_800_1280.dtsi"
#include "lcd-rpdzkj-hdmi-1920-1080.dtsi"

//#include "lcd-b101ew05.dtsi"

/ {
	compatible = "rockchip,rk3128";

	chosen {
		bootargs = "vmalloc=496M rockchip_jtag";
		//bootargs = "vmalloc=396M rockchip_jtag";
	};

	usb_control {
		compatible = "rockchip,rk3126-usb-control";
		rockchip,remote_wakeup;
		rockchip,usb_irq_wakeup;
	};
	
        rockchip_suspend {
               rockchip,ctrbits = <
                       (0
                       |RKPM_CTR_PWR_DMNS
                       |RKPM_CTR_GTCLKS
                       |RKPM_CTR_PLLS
                       //|RKPM_CTR_ARMOFF_LPMD
                       |RKPM_CTR_DDR
                       |RKPM_CTR_IDLESRAM_MD
                       |RKPM_CTR_DDR
                       //|RKPM_CTR_BUS_IDLE
                       //|RKPM_CTR_VOLTS
                       //|RKPM_CTR_VOL_PWM1
                       //|RKPM_CTR_VOL_PWM2
                       )
               >;
               rockchip,pmic-suspend_gpios = <GPIO3_C1>;
        };
};

&nandc {
	status = "disabled"; // used nand set "okay" ,used emmc set "disabled"
};

&nandc0reg {
	status = "okay"; // used nand set "disabled" ,used emmc set "okay"
};

&emmc {
	clock-frequency = <50000000>;
	clock-freq-min-max = <400000 50000000>;
	supports-highspeed;
	supports-emmc;
	bootpart-no-access;
	supports-DDR_MODE;
	ignore-pm-notify;
	keep-power-in-suspend;
	//poll-hw-reset
	status = "okay";
};

&codec {
	spk_ctl_io = <&gpio0 GPIO_D6 GPIO_ACTIVE_HIGH>;
	spk-mute-delay = <1>;
	hp-mute-delay = <1>;
        rk312x_for_mid = <1>;
	is_rk3128 = <1>;
	spk_volume = <25>;
	hp_volume = <25>;
	capture_volume = <26>;
	gpio_debug = <1>;
	codec_hp_det = <1>;
};

&i2c0 {
	status = "okay";
	rk816: rk816@1a {
		reg = <0x1a>;
		status = "okay";
	};
};

&i2s1 {
	status = "okay";
};

&clk_core_dvfs_table {
	operating-points = <
		/* KHz    uV */
		216000 925000
		408000 925000
		600000 950000
		696000 975000
		816000 1050000
		1008000 1175000
		1200000 1300000
		1296000 1350000
		1320000 1375000
		>;
	virt-temp-limit-1-cpu-busy = <
	/* target-temp	limit-freq */
		75	1008000
		85	1200000
		95	1200000
		100	1200000
		>;
	virt-temp-limit-2-cpu-busy = <
	/* target-temp	limit-freq */
		75	912000
		85	1008000
		95	1104000
		100	1200000
		>;
	virt-temp-limit-3-cpu-busy = <
	/* target-temp	limit-freq */
		75	816000
		85	912000
		95	100800
		100	110400
		>;
	virt-temp-limit-4-cpu-busy = <
	/* target-temp	limit-freq */
		75	816000
		85	912000
		95	100800
		100	110400
		>;
	temp-limit-enable = <1>;
	target-temp = <85>;
	status="okay";
};

&clk_gpu_dvfs_table {
	operating-points = <
		/* KHz    uV */
		200000 950000
		300000 975000
		400000 1075000
		//480000 1175000
		>;
	status="okay";
};

&clk_ddr_dvfs_table {
	operating-points = <
		/* KHz    uV */
		//200000 950000
		//300000 950000
		400000 1000000
		//533000 1200000
		>;

	freq-table = <
		/*status		freq(KHz)*/
		SYS_STATUS_NORMAL	400000
		SYS_STATUS_SUSPEND	400000
		SYS_STATUS_VIDEO_1080P  400000
		SYS_STATUS_VIDEO_4K     400000
		SYS_STATUS_PERFORMANCE  400000
		SYS_STATUS_DUALVIEW	400000
		SYS_STATUS_BOOST	400000
		SYS_STATUS_ISP		400000
		>;
	auto-freq-table = <
		//240000
		//324000
		396000
		//528000
		>;
	auto-freq=<0>;
	status="okay";
};

/include/ "rk816.dtsi"
&rk816 {
	gpios = <&gpio1 GPIO_B4 GPIO_ACTIVE_HIGH>, <&gpio3 GPIO_C1 GPIO_ACTIVE_LOW>;
	rk816,system-power-controller;
	rk816,support_dc_chg = <1>;/*1: dc chg; 0:usb chg*/
	io-channels = <&adc 0>;
	gpio-controller;
	#gpio-cells = <2>;

        regulators {
                rk816_dcdc1_reg: regulator@0{
                        regulator-name= "vdd_arm";
			regulator-min-microvolt = <700000>;
			regulator-max-microvolt = <1500000>;
			regulator-initial-mode = <0x1>;
			regulator-initial-state = <3>;
                        regulator-always-on;
                        regulator-state-mem {
				regulator-state-mode = <0x2>;
				regulator-state-enabled;
				regulator-state-uv = <950000>;
                        };
                };

                rk816_dcdc2_reg: regulator@1 {
                        regulator-name= "vdd_logic";
                        regulator-min-microvolt = <700000>;
                        regulator-max-microvolt = <1500000>;
                        regulator-initial-mode = <0x1>;
                        regulator-initial-state = <3>;
                        regulator-always-on;
                        regulator-state-mem {
                                regulator-state-mode = <0x2>;
                                regulator-state-enabled;
                                regulator-state-uv = <1000000>;
                        };
                };

                rk816_dcdc3_reg: regulator@2 {
                        regulator-name= "rk816_dcdc3";
                        regulator-min-microvolt = <1200000>;
                        regulator-max-microvolt = <1200000>;
                        regulator-initial-mode = <0x2>;
                        regulator-initial-state = <3>;
                        regulator-always-on;
                };

                rk816_dcdc4_reg: regulator@3 {
                        regulator-name= "vccio";
                        regulator-min-microvolt = <3000000>;
                        regulator-max-microvolt = <3300000>;
                        regulator-initial-mode = <0x1>;/*fast mode*/
                        regulator-initial-state = <3>;
                        regulator-always-on;
                        regulator-state-mem {
                                regulator-state-mode = <0x2>;
                                regulator-state-enabled;
                                regulator-state-uv = <3300000>;
                        };
                };

                rk816_ldo1_reg: regulator@4 {
                        regulator-name= "rk816_ldo1";
                        regulator-min-microvolt = <2800000>;
                        regulator-max-microvolt = <2800000>;
                        regulator-initial-state = <3>;
                        regulator-always-on;
                        regulator-state-mem {
                                regulator-state-disabled;
                                regulator-state-uv = <2800000>;
                        };
                };

                rk816_ldo2_reg: regulator@5 {
                        regulator-name= "rk816_ldo2";
                        regulator-min-microvolt = <1800000>;
                        regulator-max-microvolt = <1800000>;
                        regulator-initial-state = <3>;
                        regulator-always-on;
                        regulator-state-mem {
                                regulator-state-disabled;
                                regulator-state-uv = <1800000>;
                        };
                };

                rk816_ldo3_reg: regulator@6 {
                        regulator-name= "rk816_ldo3";
                        regulator-min-microvolt = <1100000>;
                        regulator-max-microvolt = <1100000>;
                        regulator-initial-state = <3>;
                        regulator-always-on;
                        regulator-state-mem {
                                regulator-state-enabled;
                                regulator-state-uv = <1100000>;
                        };
                };

                rk816_ldo4_reg:regulator@7 {
                        regulator-name= "rk816_ldo4";
                        regulator-min-microvolt = <1100000>;
                        regulator-max-microvolt = <1100000>;
                        regulator-initial-state = <3>;
                        regulator-always-on;
                        regulator-state-mem {
                                regulator-state-disabled;
                                regulator-state-uv = <1100000>;
                        };
                };

                rk816_ldo5_reg: regulator@8 {
                        regulator-name= "rk816_ldo5";
                        regulator-min-microvolt = <3000000>;
                        regulator-max-microvolt = <3000000>;
                        regulator-initial-state = <3>;
                        regulator-always-on;
                        regulator-state-mem {
                                regulator-state-disabled;
                                regulator-state-uv = <3000000>;
                        };
                };

                rk816_ldo6_reg: regulator@9 {
                        regulator-name= "rk816_ldo6";
                        regulator-min-microvolt = <3000000>;
                        regulator-max-microvolt = <3000000>;
                        regulator-initial-state = <3>;
                        regulator-always-on;
                        regulator-state-mem {
                                regulator-state-disabled;
                                regulator-state-uv = <3000000>;
                        };
                };
        };

        battery {
                        compatible = "rk816-battery";
                        ocv_table = < 3400 3599 3671 3701 3728 3746 3762
                                3772 3781 3792 3816 3836 3866 3910
                                3942 3971 4002 4050 4088 4132 4183>;
                        design_capacity = <4000>;
                        design_qmax = <4100>;
                        bat_res = <100>;
                        max_input_current = <2000>;
                        max_chrg_current = <1800>;
                        max_chrg_voltage = <4200>;
                        sleep_enter_current = <300>;
                        sleep_exit_current = <300>;
                        sleep_filter_current = <100>;
                        power_off_thresd = <3400>;
                        zero_algorithm_vol = <3850>;
                        max_soc_offset = <60>;
                        monitor_sec = <5>;
                        virtual_power = <0>;
                        power_dc2otg = <1>;
                        dc_det_adc = <1>;
	};
};
